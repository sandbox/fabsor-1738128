<?php

/**
 * Overview form for hyde.
 */
function hyde_overview_form() {
  $sites = HydeSiteStorage::getStorage()->listSites();
  $rows = array();
  foreach ($sites as $site) {
    $rows[] = array(
      l($site->label, 'admin/structure/hyde/site/' . $site->name),
      l(t('delete'), 'admin/structure/hyde/site/' . $site->name . '/delete'),
    );
  }
  if (!empty($rows)) {
    return array(
      '#theme' => 'table',
      '#header' => array(t('title'), t('Operations')),
      '#rows' => $rows,
    );
  }
  return t('No sites have been created yet.');
}

/**
 * Form for creating and updating renderers.
 */
function hyde_renderer_form($form, &$form_state, $renderer = NULL) {
  $form = array();
  if (!isset($renderer)) {
    $form['plugin'] = array(
      '#type' => 'select',
      '#title' => t('Type'),
      '#default_value' => isset($renderer->plugin) ? $renderer->plugin : NULL,
      '#options' => HydeRenderer::getRendererOptions(),
    );
    if (isset($form_state['values']['type'])) {
      $instance_name = $form_state['values']['plugin'];
    }
    else {
      $instance_name = current(array_keys($form['plugin']['#options']));
    }
    $instance = HydeRenderer::getInstance($instance_name);
    $renderer = new stdClass;
    $form_state['new_renderer'] = TRUE;
  }
  else {
    $instance = HydeRenderer::getInstance($renderer->plugin);
    $form_state['new_renderer'] = FALSE;
  }
  $form_state['renderer'] = $renderer;
  $form['name'] = array(
    '#title' => t('Name'),
    '#type' => 'machine_name',
    '#default_value' => isset($renderer->name) ? $renderer->name : NULL,
    '#machine_name' => array(
      'exists' => 'hyde_renderer_exists',
    ),
    '#required' => TRUE,
  );
  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => isset($renderer->label) ? $renderer->label : NULL,
    '#required' => TRUE,
  );
  $form['settings'] = array(
    '#tree' => TRUE,
    '#prefix' => '<div id="settings">',
    '#suffix' => '</div>',
  );
  $instance->form($form['settings'], $form_state);
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

function hyde_renderer_form_submit($form, &$form_state) {
  $renderer = $form_state['renderer'];
  $renderer->name = $form_state['values']['name'];
  $renderer->label = $form_state['values']['label'];
  if (!isset($renderer->plugin)) {
    $renderer->plugin = $form_state['values']['plugin'];
  }
  $renderer->settings = $form_state['values']['settings'];
  HydeRendererStorage::getStorage()->save($renderer);
}

/**
 * Form for creating and editing sites.
 */
function hyde_site_form($form, &$form_state, HydeSite $site = NULL) {
  if (!isset($site)) {
    $site = new HydeSite();
  }
  $form = array();
  $form_state['site'] = $site;
  $form['name'] = array(
    '#title' => t('Name'),
    '#type' => 'machine_name',
    '#default_value' => $site->name,
    '#machine_name' => array(
      'exists' => 'hyde_site_exists',
    ),
    '#required' => TRUE,
  );
  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $site->label,
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#description' => check_plain($site->description),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Submit handler for page creation form.
 */
function hyde_site_form_submit(&$form, &$form_state) {
  $site = $form_state['site'];
  $site->name = $form_state['values']['name'];
  $site->label = $form_state['values']['label'];
  $site->description = $form_state['values']['description'];
  HydeSiteStorage::getStorage()->save($site);
  drupal_set_message(t('The hyde site %site was created', array('%site' => $site->label)));
  $form_state['redirect'] = 'admin/structure/hyde-site/' . $site->name;
}

function hyde_site_page($site) {
  $render = array();
  drupal_set_title($site->label);
  if (!empty($site->description)) {
    $render['description'] = array(
      '#type' => 'item',
      '#title' => t('Description'),
      '#markup' => check_plain($site->description),
    );
  }
  $render['iterators'] = array(
    '#type' => 'fieldset',
    '#description' => t('Iterators fetch content from your drupal site which can be rendered.'),
    '#title' => t('Iterators'),
  );
  $render['iterators']['table'] = array(
    '#theme' => 'table',
    '#header' => array(t('Name'), t('Operations')),
    '#rows' => hyde_iterator_rows($site),
  );
  $render['iterators']['add'] = array(
    '#markup' => l(t('Add iterator'), "admin/structure/hyde/site/{$site->name}/add-iterator"),
  );
  return $render;
}

/**
 * Transform iterators to rows in a table.
 * @param type $iterators
 */
function hyde_iterator_rows($site) {
  $iterator_rows = array();
  foreach ($site->getIterators() as $iterator) {
    $iterator_rows[] = array($iterator->label, l(t('Edit'), "admin/structure/hyde/site/{$site->name}/iterator/$iterator->id"));
  }
  return $iterator_rows;
}

/**
 * Check if a particular site exits.
 * @param type $name
 * @return type
 */
function hyde_site_exists($name) {
  $site = HydeSiteStorage::getStorage()->load($name);
  return !empty($site);
}

/**
 * A basic hyde iterator form.
 */
function hyde_iterator_form($form, &$form_state, HydeSite $site, $iterator_id = NULL) {
  ctools_include('context');
  ctools_include('context-admin');
  ctools_include('plugins-admin');
  ctools_include('cache');
  ctools_context_admin_includes();

  $form_state['site'] = $site;
  
  if (!isset($iterator_id)) {
    $form['type'] = array(
      '#type' => 'select',
      '#title' => t('Type'),
      '#options' => HydeIterator::getIteratorOptions(),
    );
    $iterator = new stdClass;
    $iterator->id = $site->nextIteratorId();
    $iterator->contexts = array();
    $iterator->relationships = array();
    $form_state['new_iterator'] = TRUE;
  }
  else {
    $iterator = $site->getIterator($iterator_id);
    if (!$iterator) {
      return drupal_not_found();
    }
    $form_state['new_iterator'] = FALSE;
  }
  $form_state['iterator'] = $iterator;
  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => isset($iterator->label) ? $iterator->label : NULL,
    '#required' => TRUE,
  );

  if (isset($form_state['values']['type'])) {
    $iterator_name = $form_state['values']['type'];
  }
  elseif (isset($iterator->type)) {
    $iterator_name = $iterator->type;
  }
  else {
   $iterator_name = current(array_keys($form['type']['#options']));
  }
  $iteratorInstance = HydeIterator::getInstance($iterator_name, $iterator->settings);
  $form['iterator'] = array(
    '#tree' => TRUE,
    '#prefix' => '<div class="iterator">',
    '#suffix' => '</div>',
  );
  $iteratorInstance->getContexts();
  if (isset($iteratorInstance)) {
    $iteratorInstance->form('iterator_' . $site->name . '_' . $iterator->id, $form['iterator'], $form_state);
    $form_state['iterator_instance'] = $iteratorInstance;
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

function hyde_iterator_form_submit($form, &$form_state) {
  $site = $form_state['site'];
  $iterator = $form_state['iterator'];
  $iterator->label = $form_state['values']['label'];
  $iterator->settings = $form_state['values']['iterator'];
  $form_state['iterator_instance']->formSubmit('iterator_' . $site->name . '_' . $iterator->id, $iterator->settings, $form, $form_state['values']['iterator']);
  if (isset($form_state['values']['type'])) {
    $iterator->type = $form_state['values']['type'];
  }
  if ($form_state['new_iterator']) {
    $site->addIterator($iterator);
  }
  HydeSiteStorage::getStorage()->save($site);
}

function hyde_iterator_delete_form($form, &$form_state, HydeSite $site, $iterator_id) {
  $iterator = $site->getIterator($iterator_id);
  if (!$iterator) {
    return drupal_not_found();
  }
  $form_state['iterator_id'] = $iterator_id;
  $form_state['site'] = $site;
  return confirm_form($form,
    t('Are you sure you want to delete %label?', array('%title' => $iterator->label)),
    'admin/structure/hyde/site/' . $site->name . '/' . $iterator_id,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

function hyde_iterator_delete_form_submit($form, &$form_state) {
  $form_state['site']->removeIterator($form_state['iterator_id']);
  HydeSiteStorage::getStorage()->save($form_state['site']);
  $form_state['redirect'] = 'admin/structure/hyde/site/' . $form_state['site']->name;
}
