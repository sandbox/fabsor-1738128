<?php
/**
 * Describe a hyde site.
 */
class HydeSite {
  private $iterators = array();
  public $name;
  public $label;
  public $description;

  /**
   * Instantiate a new site.
   * @param $name
   *   (Optional) a machine-readable name.
   * @param $label
   *   (Optional) A human-readable label.
   * @param description
   *   (Optional) A description describing this site.
   */
  function __construct($name = NULL, $label = NULL, $description = NULL) {
    $this->name = $name;
    $this->label = $label;
    $this->description = $description;
    $this->settings = array();
    $this->settings['iterators'] = array();
  }

  function removeIterator($iterator) {
    if (!isset($this->settings['iterators'])) {
      return;
    }
    if (is_object($iterator)) {
      unset($this->settings['iterators'][$iterator->id-1]);
    }
    else {
      unset($this->settings['iterators'][$iterator-1]);
    }
  }

  function addIterator($iterator) {
    if (!isset($this->settings['iterators'])) {
      $this->settings['iterators'] = array();
    }
    $this->settings['iterators'][] = $iterator;
    $iterator->id = count($this->settings['iterators']);
  }
  /**
   * Get all configured iterators for this site.
   * @return HydeIterator[]
   *   An array of iterators.
   */
  function getIterators() {
    if (!isset($this->settings['iterators'])) {
      $this->settings['iterators'] = array();
    }
    return $this->settings['iterators'];
  }

  function getIterator($iterator) {
    if (!isset($this->settings['iterators'])) {
      return FALSE;
    }
    return isset($this->settings['iterators'][$iterator-1]) ? $this->settings['iterators'][$iterator-1] : FALSE;
  }

  /**
   * Get the next ID for an iterator.
   */
  function nextIteratorId() {
    return isset($this->settings['iterators']) ? count($this->settings['iterators']) + 1 : 0;
  }
}
