<?php

/**
 * Page storage interface. We don't really
 * have to abstract this really, but it's good
 * for mocking purposes.
 */
interface HydeRendererStorageInterface {

  /**
   * Save a page to the storage.
   */
  public function save($renderer);

  /**
   * Load a site from the storage.
   */
  public function load($name);

  /**
   * List all available sites.
   */
  public function listRenderers();
}

abstract class HydeRendererStorage implements HydeRendererStorageInterface {

  /**
   * Load a storage class.
   */
  static function getStorage($name = NULL, $args = NULL) {
    // Just load the HydeDatabaseRendererStorage for now.
    // this is just so that we have the possibility of
    // building out this functionality later.
    static $storage = NULL;
    if (!isset($storage)) {
      $storage = new HydeDatabaseRendererStorage();
    }
    return $storage;
  }

}

/**
 * Database storage for Hyde sites.
 */
class HydeDatabaseRendererStorage extends HydeRendererStorage {

  /**
   * Save a page to the storage.
   */
  function save($renderer) {
    ctools_include('export');
    $renderer->export_type = isset($renderer->pid) ? EXPORT_IN_DATABASE : FALSE;
    ctools_export_crud_save('hyde_renderer', $renderer);
  }

  /**
   * Load a site from the storage.
   */
  function load($name) {
    ctools_include('export');
    $result = ctools_export_load_object('hyde_renderer', 'names', array($name));
    if (isset($result[$name])) {
      return $result[$name];
    }
    return FALSE;
  }

  /**
   * List all available sites.
   */
  function listRenderers() {
    ctools_include('export');
    return ctools_export_load_object('hyde_renderer');
  }
}
