<?php

/**
 * Page storage interface. We don't really
 * have to abstract this really, but it's good
 * for mocking purposes.
 */
interface HydeSiteStorageInterface {

  /**
   * Save a page to the storage.
   */
  public function save(HydeSite $page);

  /**
   * Load a site from the storage.
   */
  public function load($name);

  /**
   * List all available sites.
   */
  public function listSites();
}

abstract class HydeSiteStorage implements HydeSiteStorageInterface {

  /**
   * Load a storage class.
   */
  static function getStorage($name = NULL, $args = NULL) {
    // Just load the HydeDatabaseSiteStorage for now.
    // this is just so that we have the possibility of
    // building out this functionality later.
    static $storage = NULL;
    if (!isset($storage)) {
      $storage = new HydeDatabaseSiteStorage();
    }
    return $storage;
  }

}

/**
 * Database storage for Hyde sites.
 */
class HydeDatabaseSiteStorage extends HydeSiteStorage {

  /**
   * Save a page to the storage.
   */
  function save(HydeSite $site) {
    ctools_include('export');
    $site->export_type = isset($site->pid) ? EXPORT_IN_DATABASE : FALSE;
    ctools_export_crud_save('hyde_site', $site);
  }

  /**
   * Load a site from the storage.
   */
  function load($name) {
    ctools_include('export');
    $result = ctools_export_load_object('hyde_site', 'names', array($name));
    if (isset($result[$name])) {
      return $result[$name];
    }
    return FALSE;
  }

  /**
   * List all available sites.
   */
  function listSites() {
    ctools_include('export');
    return ctools_export_load_object('hyde_site');
  }
}
