<?php
class HydeEntityPreprocessor {
  function __construct($language, $params) {
    $this->language = $language;
    $this->settings = $params;
    $this->settings += $this->defaultSettings();
  }

  /**
   * Default settings.
   */
  function defaultSettings() {
    return array('type' => 'node', 'renderer' => 'standard');
  }

  /**
   * Process a particular item.
   */
  function process($item) {
    // @todo Execute any access rules to determine if we need to load anything more.
    // @todo Add releationships for this item.
    list($entity_id, $revision_id, $bundle) = entity_extract_ids($this->settings['type'], $item);
    // We might have multiple translations available.
    // This processor only operates on one particular language at a time.
    $item->fields = array();
    foreach (field_info_instances($this->settings['type'], $bundle) as $instance) {
      if (isset($item->{$instance['field_name']}[$this->language])) {
        $item->fields[$instance['field_name']] = $item->{$instance['field_name']}[$this->language];
      }
      elseif (isset($item->{$instance['field_name']}['und'])) {
        $item->fields[$instance['field_name']] = $item->{$instance['field_name']}['und'];
      }
    }
    // Alright, time for some rendering fun.
    hyde_get_renderer($this->settings['renderer'])->render($item);
  }

  /**
   * Return a FAPI array that can be rendered.
   */
  function toFAPI($item) {
    return entity_view($this->settings['type'], $item);
  }
}