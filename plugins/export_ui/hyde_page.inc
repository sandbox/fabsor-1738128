<?php
$plugin = array(
  'schema' => 'hyde_page',
  'access' => 'administer sites',
  'create access' => 'create hyde site',
  'menu' => array(
    'menu item' => 'hyde-site',
    'menu title' => 'Sites',
    'menu description' => 'Add, edit or delete hyde sites.',
  ),

  'title singular' => t('site'),
  'title singular proper' => t('Site'),
  'title plural' => t('sites'),
  'title plural proper' => t('Sites'),

  'handler' => array(
    'class' => 'HydePageUI',
    'parent' => 'ctools_export_ui',
  ),
  'use wizard' => TRUE,
  'form info' => array(
    'order' => array(
      'basic' => t('Settings'),
      'iterators' => t('Iterators'),
      'processors' => t('Processors'),
      'renderers' => t('Renderers'),
    ),
    // We have to add this form specially because it's invisible.
    'forms' => array(
      'move' => array(
        'form id' => 'ctools_export_ui_edit_item_wizard_form',
      ),
    ),
  ),
);

