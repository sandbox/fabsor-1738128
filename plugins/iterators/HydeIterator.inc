<?php

/**
 * Base class for iterators.
 */
abstract class HydeIterator implements Iterator {
  var $settings = array();

  function __construct($settings) {
    $this->settings = $settings;
  }

  static function getIterators() {
    ctools_include('plugins');
    return ctools_get_plugins('hyde', 'iterators');
  }

  static function getIterator($name) {
    ctools_include('plugins');
    return ctools_get_plugins('hyde', 'iterators', $name);
  }

  static function getInstance($name, $params = array()) {
    $plugin = HydeIterator::getIterator($name);
    if (!empty($plugin)) {
      return new $plugin['class']($params);
    }
  }

  static function getIteratorOptions() {
    $iterators = HydeIterator::getIterators();
    $options = array();
    foreach ($iterators as $name => $iterator) {
      $options[$name] = $iterator['label'];
    }
    return $options;
  }

  function defaultSettings() {
    $renderers = HydeRendererStorage::getStorage()->listRenderers();
    return array(
      'renderer' => current(array_keys($renderers)),
      'renderer_settings' => array(),
      'contexts' => array(),
      'relationships' => array(),
    );
  }

  function getSetting($name) {
    if (isset($this->settings[$name])) {
      return $this->settings[$name];
    }
    $default_settings = $this->defaultSettings();
    return $default_settings[$name];
  }

  function getSettings() {
    return $this->settings + $this->defaultSettings();
  }


  function getContexts() {
    if (!isset($this->contexts)) {
      $ctools_settings = (object)$this->getSettings();
      $this->contexts = ctools_context_load_contexts($ctools_settings);
    }
    return $this->contexts;
  }

  /**
   * Extend the iterator form with this method.
   * @param type $form
   * @param type $form_state
   */
  function form($iterator_id, &$form, &$form_state) {
    $renderers = HydeRendererStorage::getStorage()->listRenderers();
    $options = array();
    foreach ($renderers as $renderer => $info) {
      $options[$renderer] = $info->label;
    }
    $form['renderer'] = array(
      '#type' => 'select',
      '#default_value' => $this->getSetting('renderer'),
      '#options' => $options,
      '#title' => t('Renderer'),
    );
    if (isset($form_state['renderer'])) {
      $renderer_name = $renderer;
    }
    else {
      $renderer_name = $this->getSetting('renderer');
    }
    $renderer = HydeRendererStorage::getStorage()->load($renderer_name);
    $form['renderer_settings'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => t('Renderer settings'),
      '#prefix' => '<div id="renderer-settings">',
      '#prefix' => '</div>',
    );
    $instance = HydeRenderer::getInstance($renderer->plugin, $renderer->settings);
    $instance->itemForm($this->getSetting('renderer_settings'), $form['renderer_settings'], $form_state);
    if (!$form_state['new_iterator']) {
      ctools_include('context-admin');
      ctools_include('plugins-admin');
      ctools_include('cache');
      ctools_include('cache');
      ctools_context_admin_includes();
      $ctools_settings = ctools_cache_get('simple', $iterator_id);
      if (empty($ctools_settings)) {
        $ctools_settings = (object)$this->getSettings();
        ctools_cache_set('simple', $iterator_id, $ctools_settings);
      }
      // Add context form.
      ctools_context_add_context_form('simple', $form, $form_state, $form['contexts'], $ctools_settings, $iterator_id);
      ctools_context_add_relationship_form('simple', $form, $form_state, $form['relationships'], $ctools_settings, $iterator_id);
    }
  }

  function formSubmit($iterator_id, &$settings, $form, $form_state) {
    $ctools_settings = ctools_cache_get('simple', $iterator_id);
    if (!empty($ctools_settings)) {
      $settings['contexts'] = $ctools_settings->contexts;
      $settings['relationships'] = $ctools_settings->relationships;
    }
    else {
      $settings['contexts'] = $this->getSetting('contexts');
      $settings['contexts'] = $this->getSetting('relationships');
    }
    ctools_cache_clear('simple', $iterator_id);
  }

  function getRenderer() {
    if (!isset($this->renderer)) {
      $renderer_name = $this->settings['renderer'];
      $renderer = HydeRendererStorage::getStorage()->load($renderer_name);
      $this->renderer = HydeRenderer::getInstance($renderer->plugin, $renderer->settings);
    }
    return $this->renderer;
  }
  /**
   * Export all items that this iterator iterates over.
   */
  function export($output_dir) {
    $renderer = $this->getRenderer();
    foreach ($this as $item) {
      $result = $renderer->render($this->getSetting('renderer_settings'), $item);
      $path = $output_dir . '/' . $this->getFileName($item);
      // Make sure the folders exist.
      $dir = dirname($path);
      if (!empty($dir) && !is_dir($dir)) {
        mkdir($dir, 0777, TRUE);
      }
      file_put_contents($path, $result);
    }
  }

  function getTemplate() {
    return $this->settings['renderer_settings']['template'];
  }
}