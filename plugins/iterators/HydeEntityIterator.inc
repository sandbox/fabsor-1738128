<?php

/**
 * An entity iterator.
 */
class HydeEntityIterator extends HydeIterator {
  public function __construct($settings = array()) {
    ctools_include('context');
    $this->settings = $settings;
    $type = $this->getSetting('type');
    $base_context = ctools_context_create_empty('entity:' . $type);
    $base_context->identifier = $base_context->title = $this->getSetting('type');
    $this->settings['base_contexts'][] = $base_context;
    $this->position = 0;
    $this->page = 1;
    $this->entities = array();
  }

  function defaultSettings() {
    return parent::defaultSettings() + array(
      'type' => 'node',
      'url_pattern' => 'node/%node:nid',
      'entity_limit' => 100,
    );
  }

  public function query() {
    if (!isset($this->db_query)) {
      $query = new EntityFieldQuery();
      $bundle = $this->getSetting('bundle');
      $query->entityCondition('entity_type', $this->getSetting('type'));
      if (isset($bundle)) {
        $query->entityCondition('bundle', $bundle);
      }
      $this->db_query = $query;
    }
    return $this->db_query;
  }

  function execute() {
    if (!isset($this->result)) {
      $query = $this->query();
      $result = $query->execute();
      $this->result = isset($result[$this->getSetting('type')]) ? array_keys($result[$this->getSetting('type')]) : array();
    }
    return $this->result;
  }

  function form($iterator_id, &$form, &$form_state) {
    parent::form($iterator_id, $form, $form_state);
    $entity_info = entity_get_info();
    $entity_options = array();
    foreach ($entity_info as $name => $info) {
      $entity_options[$name] = $info['label'];
    }
    $form['type'] = array(
      '#title' => t('Type'),
      '#type' => 'select',
      '#options' => $entity_options,
      '#default_value' => $this->getSetting('type'),
      '#description' => t('The entity type to export.'),
    );
    $form['url_pattern'] = array(
      '#type' => 'textfield',
      '#title' => t('URL pattern'),
      '#default_value' => $this->getSetting('url_pattern'),
      '#description' => t('The pattern that will be used. Token replacements are available.'),
    );
  }

  /**
   * Get the filename of a particular item.
   */
  function getFilename($item) {
    $type = $this->getSetting('type');
    list($id) = entity_extract_ids($type, $item[$type]);
    return $type . '/' . $id . '.html';
  }

  function count() {
    // Get the query and count it.
    if (!isset($this->row_count)) {
      $query = $this->query();
      $this->row_count = $query->count()->execute();
    }
    return $this->row_count;
  }

  function rewind() {
    unset($this->db_query);
    unset($this->result);
    $this->position = 0;
    $this->page = 1;
  }

  function current() {
    $this->execute();
    $span =  $this->page > 1 ? $this->page * $this->getSetting('entity_limit') : 0;
    // Fetch more entities if required.
    if (!isset($this->entities[$this->position - $span])) {
      $ids = array_splice($this->result, $span);
      $this->entities = array_values(entity_load($this->getSetting('type'), $ids));
    }
    // Load all contexts for this item.
    $this->settings['base_contexts'][0] = ctools_context_create('entity:' . $this->getSetting('type'), $this->entities[$this->position - $span]);
    $contexts = $this->getContexts();
    $variables = array();
    foreach ($contexts as $name => $context) {
      $variables[$context->keyword] = $context->data;
    }
    return $variables;
  }

  function key() {
    return $this->position;
  }

  function next() {
    $this->execute();
    ++$this->position;
  }

  function valid() {
    $this->execute();
    return $this->count() > $this->position;
  }
}
