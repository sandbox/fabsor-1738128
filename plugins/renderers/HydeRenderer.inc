<?php
abstract class HydeRenderer {
    function __construct($settings) {
    $this->settings = $settings;
  }

  static function getRenderers() {
    ctools_include('plugins');
    return ctools_get_plugins('hyde', 'renderers');
  }

  static function getRenderer($name) {
    ctools_include('plugins');
    return ctools_get_plugins('hyde', 'renderers', $name);
  }

  /**
   * A form.
   * @param type $form
   * @param type $form_state
   */
  function form(&$form, &$form_state) {
    
  }

  /**
   * Get an instance of a renderer plugin.
   * @param type $name
   * @param type $params
   * @return HydeRenderer
   *   An instance of the renderer you want.
   */
  static function getInstance($name, $params = array()) {
    $plugin = HydeRenderer::getRenderer($name);
    if (!empty($plugin)) {
      return new $plugin['class']($params);
    }
  }

  static function getRendererOptions() {
    $renderers = HydeRenderer::getRenderers();
    $options = array();
    foreach ($renderers as $name => $iterator) {
      $options[$name] = $iterator['label'];
    }
    return $options;
  }
}
