<?php

/**
 * Render data using the Twig rendering engine.
 */
class HydeTwigRenderer extends HydeRenderer {

  /**
   * @param settings
   *   Settings for this renderer.
   *   - path: the path where the templates are stored.
   *   - cache_path: the cache path.
   */
  function __construct(array $settings) {
    $this->settings = $settings;
    $this->settings += $this->defaultSettings();
    // Register the autoloader.
    require_once drupal_get_path('module', 'hyde')  . '/vendor/twig/twig/lib/Twig/Autoloader.php';
    Twig_Autoloader::register();
  }

  /**
   * Default settings.
   */
  function defaultSettings() {
    return array(
      'path' => '',
      'cache_path' => '/tmp',
    );
  }

  function form(&$form, &$form_state) {
    $paths = array();
    // Load all active themes.
    $themes = system_list('theme');
    foreach ($themes as $theme => $info) {
      if (isset($info->info['twig_path'])) {
        $paths[drupal_get_path('theme', $theme) . '/' . $info->info['twig_path']] = $info->info['name'];
      }
    }
    $form['path'] = array(
      '#type' => 'select',
      '#options' => $paths,
      '#title' => t('Twig path'),
      '#description' => t('The path to the twig template files.')
    );
    $form['cache_path'] = array(
      '#type' => 'textfield',
      '#title' => t('cache path'),
      '#description' => t('The path to the twig template files.')
    );
  }

  /**
   * Configuration form for rendering a particular item.
   */
  function itemForm($settings, &$form, &$form_state) {
    $form['template'] = array(
      '#type' => 'select',
      '#title' => t('Template'),
      '#default_value' => isset($settings['template']) ? $settings['template'] : NULL,
      '#options' => $this->getAvailableTemplates(),
    );
  }

  function getAvailableTemplates() {
    $loader = new Twig_Loader_Filesystem($this->settings['path']);;
    $options = array();
    foreach ($loader->getPaths() as $path) {
      $templates = file_scan_directory($path, '/(\.html|html.twig|twig)$/');
      foreach ($templates as $template) {
        $options[$template->filename] = $template->name;
      }
    }
    return $options;
  }

  /**
   * Render a twig template.
   * @param $template
   *  The template file to render.
   * @param $data
   *   the data to pass to the template.
   */
  function render($item_settings, $data) {
    $this->loader = new Twig_Loader_Filesystem($this->settings['path']);
    $this->twig = new Twig_Environment($this->loader, array('cache' => $this->settings['cache_path']));
    // Render a template.
    $template = $this->twig->loadTemplate($item_settings['template']);
    return $template->render($data);
  }
}
