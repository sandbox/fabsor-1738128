<?php

function hyde_drush_command() {
  $items = array();
  $items['hyde-export'] = array(
    'aliases' => array('hex'),
    'description' => 'Export a hyde site to HTML',
    'arguments' => array(
      'site'  => 'name of the site to export',
      'output_dir' => 'Name of the directory relative to the site root to output the site to.',
    ),
    'examples' => array(
      'drush hex mysite output' => 'Exports a hyde site to a directory called output in your drupal root.',
    ),
  );
  return $items;
}

function drush_hyde_export($site_name, $output_dir) {
  $site = hyde_site_load($site_name);
  if (!is_dir($output_dir)) {
    mkdir($output_dir);
  }
  foreach ($site->getIterators() as $iterator) {
    $instance = HydeIterator::getInstance($iterator->type, $iterator->settings);
    $instance->export($output_dir);
    unset($instance);
  }
}
