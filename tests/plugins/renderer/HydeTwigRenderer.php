<?php

class HydeTwigRendererTestCase extends PHPUnit_Framework_TestCase {
  /**
   * Require the file.
   */
  function __construct() {
    require_once('../../../plugins/renderer/HydeTwigRenderer.class.php');
  }
  /**
   * Test the rendering function.
   */
  function testRender() {
    $settings = array(
      'path' => 'twig',
      'cache_path' => '/tmp',
    );
    $renderer = new HydeTwigRenderer($settings);
    $data = array('title' => 'My title');
    $rendered = $renderer->render('test.html', $data);
    $this->assertEquals($rendered, "My title\n");
  }

}