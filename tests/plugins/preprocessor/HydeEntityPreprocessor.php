<?php

class HydeEntityPreprocessorTestCase extends PHPUnit_Framework_TestCase {

 /**
   * Require the file.
   */
  function __construct() {
    require_once('../../../plugins/preprocessor/HydeTwigPreprocessorTestCase.class.php');
  }

  function testProcessor() {
    // Mock a fake node.
    $node = (object)array(
      'title' => 'Title',
      'nid' => 1,
      'field_body' => array(
        'en' => array(
          array(
            'value' => 'Lorem ipsum',
          )
        )
      ),
    );
    $nodes = array(
      $node,
      $node,
    );
    $settings = array(
      'language' => 'en',
      'type' => 'node',
      'renderer' => 'HydeTwigRenderer',
      'renderer_settings' => array(
        'template' => 'node.html',
      ),
    );
    $processor = HydeTwigPreprocessor($settings);
    $node = $processor->process($node);
  }
}